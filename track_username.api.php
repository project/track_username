<?php

/**
 * @file
 * Hooks provided by the Track Username module.
 */

/**
 * Alter the track limit for a user's tracked username prior to tracking.
 *
 * @param string $track_limit
 *   The username track limit string prior to tracking.
 * @param object $account
 *   A user account object.
 *
 * @see track_username_update()
 *
 * @ingroup track_username
 */
function hook_track_username_count_alter(&$track_limit, $account) {

}

/**
 * Alter a user's new username before it is saved to the database.
 *
 * @param string $new_username
 *   The user's new username.
 * @param object $account
 *   A user account object.
 *
 * @see track_username_update()
 *
 * @ingroup track_username
 */
function hook_track_username_alter(&$new_username, $account) {

}

/**
 * Respond to updates to an account's new username.
 *
 * @param string $new_username
 *   The user's new username.
 * @param object $account
 *   A user account object.
 *
 * @see track_username_update()
 *
 * @ingroup track_username
 */
function hook_track_username_update($new_username, $account) {

}
