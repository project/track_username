The Track Username module allows the administrator to keep track of the
user's username history, with configurable limit. Includes a function to
retrieve tracked username history for further module integration (ie. limit
access based on username update count).

Installation
------------
Standard module installation applies.

Menus
-----
The only menu item is for the settings page.

Settings
--------
The settings page is at Administer >> Config >> User >> Track Username.

This is where you choose the track history limit.

Permissions
-----------
The settings page is controlled by the "administer track_username"
permission.
