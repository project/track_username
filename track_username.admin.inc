<?php

/**
 * @file
 * Track Username configuration UI.
 */

/**
 * Form constructor for the Track Username settings form.
 */
function track_username_settings_form($form, &$form_state) {
  $form['track_username_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('History limit'),
    '#description' => t('Limit the tracked username history per user. Leave empty for unlimited (not recommended).<br>WARNING: changing this value will delete all existing tracked usernames!!!'),
    '#size' => 4,
    '#default_value' => variable_get('track_username_limit', 10),
    '#element_validate' => array('element_validate_integer_positive'),
  );

  $form['#submit'][] = 'track_username_settings_form_submit';
  return system_settings_form($form);
}

/**
 * Form submission handler for track_username_settings_form().
 */
function track_username_settings_form_submit($form, $form_state) {
  if ($form['track_username_limit']['#default_value'] != $form_state['values']['track_username_limit']) {
    // Only clear the track_username cache if the limit was changed.
    track_username_delete_all();
  }
}
